#+TITLE:      Create/Attach byobu session
#+AUTHOR:     Colin Noel Bell
#+EMAIL:      col@baibell.org
#+STARTUP:    hideall
#+OPTIONS:    toc:1 h:1
#+PROPERTY:   header-args  :tangle ~/bin/ba
#+PROPERTY:   header-args+ :comments both :mkdirp yes
#+PROPERTY:   header-args+ :shebang "#!/bin/sh"

Create/Attach byobu session with the same name as the current directory.

#+BEGIN_SRC sh
  if grep -q 'BYOBU_BACKEND=screen' ~/.byobu/backend; then
    byobu -dRS `basename $PWD`
  else
    byobu new-session -As `basename $PWD`
  fi
#+END_SRC
